<div class="records form">
<?php echo $this->Form->create('Record'); ?>
	<fieldset>
		<legend><?php echo __('Edit Record'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('message', array('label' => __('Message')));
		echo $this->Form->input('date', array('label' => __('Date')));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Record.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Record.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Records'), array('action' => 'index')); ?></li>
	</ul>
</div>
