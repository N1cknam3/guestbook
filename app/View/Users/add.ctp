<div class="users form">
	<?php echo $this->Form->create('User'); ?>
	    <fieldset>
	        <legend><?php echo __('Registering new User'); ?></legend>
		    <?php
		    	echo $this->Form->input('username', array('label' => __('Username')));
		        echo $this->Form->input('password', array('label' => __('Password')));
		    ?>
	    </fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>