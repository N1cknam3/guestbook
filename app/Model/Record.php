<?php
App::uses('AppModel', 'Model');
/**
 * Record Model
 *
 * @property User $User
 */
class Record extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'message' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please fill that field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'noBadWords' => array(
				'rule' => array('noBadWords', 'message'),
				'message' => 'Here should be no bad words',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

/**
 * Check string for bad words
 *
 * @return false if there is no bad words
 */
	public function noBadWords($check, $field) {
		$badWords = array('плохо', 'дичь');
		$check = mb_strtolower($check[$field]);
		foreach ($badWords as $word) {
			if (strrpos($check, $word) !== false)
				return false;
		}
		return true;
	}

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * Check if record belongs to user
 *
 * @param int $recordId
 * @param int $userId
 */
	public function isOwnedBy($recordId, $userId) {
	    return $this->field('id', array('id' => $recordId, 'user_id' => $userId)) !== false;
	}
}
