<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
        'Flash',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'records',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authorize' => array('Controller')
        ),
        'Session',
        'Cookie'
    );

    public function beforeFilter() {
        $this->Auth->allow('login', 'rus', 'eng');
        $this->set('authUser', $this->Auth->user());

        $this->_setLanguage();
    }

/*
 * Check prefer language in cookie and save it into current session
 */
    private function _setLanguage() {
        if ($this->Cookie->check('lang')) {
            $this->Session->write('Config.language', $this->Cookie->read('lang'));
        }
        if ($this->Session->check('Config.language')) {
            Configure::write('Config.language', $this->Session->read('Config.language'));
        }
    }

/*
 * Set russian language in cookie and current session both (if user switch off cookies)
 */
    public function rus() {
        $this->Cookie->write('lang', 'rus', false, '20 days');
        $this->Session->write('Config.language', 'rus');
        $this->redirect($this->referer());
    }

/*
 * Set english language in cookie and current session both (if user switch off cookies)
 */
    public function eng() {
        $this->Cookie->write('lang', 'eng', false, '20 days');
        $this->Session->write('Config.language', 'eng');
        $this->redirect($this->referer());
    }

}
