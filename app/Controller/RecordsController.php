<?php
App::uses('AppController', 'Controller');
/**
 * Records Controller
 *
 * @property Record $Record
 * @property PaginatorComponent $Paginator
 */
class RecordsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * Check if user can access actions 
 * For editing and deleting only their records
 *
 * @param int $recordId
 * @param int $userId
 */
	public function isAuthorized($user) {
	    // All registered users can add and view records
	    if (in_array($this->action, array('add', 'index', 'view'))) {
	        return true;
	    }

	    // The owner of a record can edit and delete it
	    if (in_array($this->action, array('edit', 'delete'))) {
	        $recordId = (int) $this->request->params['pass'][0];
	        if ($this->Record->isOwnedBy($recordId, $user['id'])) {
	            return true;
	        } else {
	        	$this->Flash->error(__('Only the owner of a record can edit and delete it'));
	        }
	    }
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Record->recursive = 0;
		$this->set('records', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Record->exists($id)) {
			throw new NotFoundException(__('Invalid record'));
		}
		$options = array('conditions' => array('Record.' . $this->Record->primaryKey => $id));
		$this->set('record', $this->Record->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Record->create();
        	$this->request->data['Record']['user_id'] = $this->Auth->user('id');
			if ($this->Record->save($this->request->data)) {
				$this->Flash->success(__('The record has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The record could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Record->exists($id)) {
			throw new NotFoundException(__('Invalid record'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Record->save($this->request->data)) {
				$this->Flash->success(__('The record has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The record could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Record.' . $this->Record->primaryKey => $id));
			$this->request->data = $this->Record->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Record->id = $id;
		if (!$this->Record->exists()) {
			throw new NotFoundException(__('Invalid record'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Record->delete()) {
			$this->Flash->success(__('The record has been deleted.'));
		} else {
			$this->Flash->error(__('The record could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
