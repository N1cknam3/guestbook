# Информационная система "Гостевая книга"

Инструкция по установке:

1. Закинуть проект в DocumentRoot вашего сервера
2. Импортировать базу данных (файл ```db_dumps/guestbook.sql```)
3. Настроить ```app/Config/database.php``` для работы с MySQL:

```
#!php

public $default = array(
	'datasource' => 'Database/Mysql',
	'persistent' => false,
	'host' => 'localhost',
	'login' => 'логин',
	'password' => 'пароль',
	'database' => 'guestbook',
	'prefix' => '',
	'encoding' => 'utf8',
);
```

### What is this repository for? ###

Have been created to show back-end skills to the NOVA\TEST PR managers.

### How do I get set up? ###

It's just a HTML page. Open in any browser